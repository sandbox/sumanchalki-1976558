<?php
/**
 * @file
 * This file is for magic password administrator password set form
 */

function _magic_password_set_password_form() {
  $form = array();
  
  $form['magic_password'] = array(
    '#type' => 'textfield', 
    '#title' => t('Magic Password'),
    '#default_value' => variable_get('magic_password_value', 'helloworld'),
    '#required' => TRUE,
  );
  
  $form['#submit'][] = '_magic_password_set_password_form_submit';

  return system_settings_form($form);
}


function _magic_password_set_password_form_submit($form, &$form_state) {
  if ($form_state['values']['magic_password']!='') { 
    variable_set('magic_password_value', $form_state['values']['magic_password']);
  }
}

